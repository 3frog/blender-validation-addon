import bpy
import math
from typing import Tuple

from blender import objectUtil
from blender.rendering.animationUtil import AnimationUtil


class Camera(object):
    DEFAULT_CAMERA_NAME = 'Camera'

    def __init__(self, camera_name: str = DEFAULT_CAMERA_NAME):
        self.name = camera_name
        self.blender_camera = bpy.data.objects.get(camera_name)

        if not self.blender_camera:
            raise ValueError('No camera with name {} found in blend file'.format(camera_name))

    @staticmethod
    def exists(camera_name):
        return camera_name in bpy.data.objects

    def get_location(self) -> Tuple[float, float, float]:
        return tuple(self.blender_camera.location)

    def set_location(self, location: Tuple[float, float, float]):
        self.blender_camera.location[0] = location[0]
        self.blender_camera.location[1] = location[1]
        self.blender_camera.location[2] = location[2]

    def look_at_location(self, target: Tuple[float, float, float]):
        bpy.ops.mesh.primitive_plane_add()
        tracking_plane = bpy.context.scene.objects.active
        tracking_plane.location[0] = target[0]
        tracking_plane.location[1] = target[1]
        tracking_plane.location[2] = target[2]

        self.blender_camera.select = True
        bpy.ops.object.track_set(type='TRACKTO')
        bpy.context.scene.objects.active = self.blender_camera
        bpy.ops.object.visual_transform_apply()

        objectUtil.delete(tracking_plane)

    def configure_angle(self, horizontal_fov_rad: float):
        self.blender_camera.data.sensor_width = 35
        self.blender_camera.data.lens_unit = 'FOV'
        self.blender_camera.data.angle = horizontal_fov_rad

    def add_keyframe(self, keyframe: int):
        self.blender_camera.select = True
        bpy.context.scene.objects.active = self.blender_camera
        self.blender_camera.keyframe_insert(data_path="location", frame=keyframe)
        self.blender_camera.keyframe_insert(data_path="rotation_euler", frame=keyframe)

        # custom property workaround, because "camera.data.angle is not animatable"
        self.blender_camera['angle'] = self.blender_camera.data.angle
        self.blender_camera.keyframe_insert(data_path='["angle"]', frame=keyframe)

    def activate(self):
        self.blender_camera.select = True
        bpy.context.scene.objects.active = self.blender_camera
        bpy.context.scene.camera = self.blender_camera

    def activate_keyframe(self, keyframe):
        bpy.context.scene.frame_set(keyframe)

        if self.blender_camera.get('angle') is not None:
            print('Using keyframe specific camera angle value: ' + str(self.blender_camera['angle']))
            self.blender_camera.data.sensor_width = 35
            self.blender_camera.data.lens_unit = 'FOV'
            self.blender_camera.data.angle = self.blender_camera['angle']
        else:
            # FIXME: use decent defaults, also change in startup blend
            print('No keyframe specific camera lens value found, using default')

    def get_keyframes(self, start_keyframe=0, end_keyframe=999):
        return AnimationUtil.get_keyframes(self.blender_camera, start_keyframe, end_keyframe)

    def find_next_available_keyframe(self):
        return AnimationUtil.find_next_available_keyframe(self.blender_camera)

    def clear_all_keyframes(self):
        AnimationUtil.clear_all_keyframes(self.blender_camera)

    def get_internal_position(self):
        return self.blender_camera.location, self.blender_camera.rotation_euler, self.blender_camera.matrix_world

    def is_vertically_straight(self):
        camera_x_rotation, camera_y_rotation = self.get_x_y_rotation()
        return (camera_x_rotation == 90 and camera_y_rotation == 0) or \
               (camera_x_rotation == -90 and camera_y_rotation == 180) or \
               (camera_x_rotation == -90 and camera_y_rotation == -180)

    def get_x_y_rotation(self):
        camera_x_rotation = round(math.degrees(self.blender_camera.rotation_euler[0]))
        camera_y_rotation = round(math.degrees(self.blender_camera.rotation_euler[1]))
        return camera_x_rotation, camera_y_rotation

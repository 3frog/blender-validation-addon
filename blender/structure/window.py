import re
import bpy

from blender.blenderfactory import BlenderFactory
from blender import objectUtil
from blender.blenderMaterialUtil import BlenderMaterialUtil


class Window(object):

    WINDOW_EXPRESSION = re.compile('^(?:.*\|)?(\d*)\.(.*)\.window\.(\d+)$')

    def __init__(self, empty_object):
        self.empty_object = empty_object

    @property
    def name(self):
        return self.empty_object.name

    @property
    def children(self):
        return self.empty_object.children

    def get_window_glass(self):
        return self.__get_child_by_object_type('WINDOW_GLASS')

    def get_window_frame(self):
        return self.__get_child_by_object_type('WINDOW_FRAME')

    def get_door_frame(self):
        return self.__get_child_by_object_type('DOOR_FRAME')

    def __get_child_by_object_type(self, object_type):
        children = [child for child in self.empty_object.children if
                  child.material_slots and child.material_slots[0].material.name.startswith(object_type)]
        if children:
            return children[0]

        return None

    @staticmethod
    def get_windows():
        return [Window(o) for o in bpy.context.scene.objects if Window.is_window(o)]

    @staticmethod
    def is_window(o):
        return o.type == 'EMPTY' and Window.WINDOW_EXPRESSION.fullmatch(o.name)

    @staticmethod
    def cleanup_windows():
        bpy.ops.wm.addon_enable(module='mesh_offset_edges')

        for window in Window.get_windows():
            window_mesh = objectUtil.join_children_recursively(window.empty_object)
            # Move mesh to Empty layer, needed if child glass was already on layer 9
            window_mesh.layers = window.empty_object.layers

            if window_mesh:
                print('Window mesh found, start cleanup ' + window.name + ': ' + window_mesh.name)
                BlenderMaterialUtil.split_by_material(window_mesh)

                found_plastic = False
                for window_part in window.children:
                    if window_part.material_slots:
                        window_part_material_name = window_part.material_slots[0].material.name
                        if window_part_material_name == 'WINDOW_PLASTIC':
                            objectUtil.delete(window_part)
                            found_plastic = True

                for window_part in window.children:
                    if window_part.material_slots:
                        window_part_material_name = window_part.material_slots[0].material.name
                        if window_part_material_name == 'WINDOW_SILL':
                            window_part.name = window.name + '.sill'
                        elif window_part_material_name == 'WINDOW_FRAME':
                            window_part.name = window.name + '.frame'
                        elif window_part_material_name.startswith('WINDOW_GLASS'):
                            window_part.name = window.name + '.glass'
                            if found_plastic:
                                Window.__offset_edges(window_part, 0.02)

    @staticmethod
    def __offset_edges(object, width):
        BlenderFactory.make_active(object)
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.offset_edges(geometry_mode='extrude', width=width, follow_face=True, caches_valid=False)
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')

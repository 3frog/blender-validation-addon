import math
import re
from mathutils import Matrix

import bpy

from blender import objectUtil
from blender.materialReplacer import MaterialReplacer


#TODO: This has become a utility class for all material handling. Pull apart and move to phases where it makes sense

class MaterialPreparer(object):
    def __init__(self, blender_factory):
        self.__blender_factory = blender_factory

    def split_all_by_material(self):
        # self.__blender_factory.show_all_layers()
        bpy.ops.object.select_all(action='DESELECT')

        multi_material_objects = (o for o in self.__blender_factory.get_visible_meshes() if len(o.material_slots) > 1)
        for o in multi_material_objects:
            self.__blender_factory.split_by_material(o)

    def get_meshes_with_materials(self):
        meshes_with_materials = (o for o in self.__blender_factory.get_visible_meshes() if len(o.material_slots) > 0 and o.material_slots[0].name)
        return meshes_with_materials

    def move_by_material_to_layer(self, material_name, layer):
        for o in self.__blender_factory.get_by_material(material_name):
            self.__blender_factory.set_to_layer(o, layer)

    def move_by_material_to_layer_contains(self, material_name, layer):
        for o in self.__blender_factory.get_by_material_contains(material_name):
            self.__blender_factory.set_to_layer(o, layer)

    def remove_by_material(self, material_name):
        bpy.ops.object.select_all(action='DESELECT')
        for o in self.__blender_factory.get_by_material(material_name):
            o.select = True
        bpy.ops.object.delete()

    def replace_archimesh_materials(self):
        MaterialReplacer.replace_material('Door_material', 'DOOR_FRAME')
        MaterialReplacer.replace_material('Window_material', 'WINDOW_FRAME')
        MaterialReplacer.replace_material('DoorGlass_material', 'DOOR_GLASS')
        MaterialReplacer.replace_material('Handle_material', 'DOOR_HANDLE')
        MaterialReplacer.replace_material('PVC', 'WINDOW_FRAME')
        MaterialReplacer.replace_material('Window_material', 'WINDOW_FRAME')
        MaterialReplacer.replace_material('Plastic', 'WINDOW_PLASTIC')
        MaterialReplacer.replace_material('Plastic_material', 'OBJECT_CHROME')
        MaterialReplacer.replace_material('Plastic_Handle_material', 'DOOR_HANDLE')
        MaterialReplacer.replace_material('Glass', 'WINDOW_GLASS')
        MaterialReplacer.replace_material('Glass_material', 'WINDOW_GLASS')
        MaterialReplacer.replace_material('Marble', 'WINDOW_SILL')

    def convert_archimesh_windows(self):
        bpy.ops.wm.addon_enable(module='mesh_offset_edges')

        for window in self.find_windows():
            window_mesh = self.__join_recursively(window)
            # Move mesh to Empty layer, needed if child glass was already on layer 9
            window_mesh.layers = window.layers

            if window_mesh:
                print('Window mesh found, start cleanup ' + window.name + ': ' + window_mesh.name)
                self.__blender_factory.split_by_material(window_mesh)

                found_plastic = False
                for window_part in self.__blender_factory.get_children(window):
                    if window_part.material_slots:
                        window_part_material_name = window_part.material_slots[0].material.name
                        if window_part_material_name == 'WINDOW_PLASTIC':
                            objectUtil.delete(window_part)
                            found_plastic = True

                for window_part in self.__blender_factory.get_children(window):
                    if window_part.material_slots:
                        window_part_material_name = window_part.material_slots[0].material.name
                        if window_part_material_name == 'WINDOW_SILL':
                            window_part.name = window.name + '.sill'
                        elif window_part_material_name == 'WINDOW_FRAME':
                            window_part.name = window.name + '.frame'
                        elif window_part_material_name.startswith('WINDOW_GLASS'):
                            window_part.name = window.name + '.glass'
                            if found_plastic:
                                self.__offset_edges(window_part, 0.02)

    @staticmethod
    def find_windows():
        window_expression = re.compile('^(\d*)\.(.*)\.window\.(\d+)$')
        return [o for o in bpy.context.scene.objects if o.type == 'EMPTY' and window_expression.fullmatch(o.name)]

    def find_window_glass(self, window):
        window_glass_objects = [child for child in window.children if len(child.material_slots) > 0 and child.material_slots[0].material.name.startswith('WINDOW_GLASS')]
        if not window_glass_objects:
            raise ValueError('Found window without a child with WINDOW_GLASS object: ' + window.name)
        if len(window_glass_objects) > 1:
            raise ValueError('Found window with multiple WINDOW_GLASS child objects: ' + window.name)

        return window_glass_objects[0]

    def __join_recursively(self, parent):
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.scene.objects.active = parent
        bpy.ops.object.select_grouped(type='CHILDREN_RECURSIVE')
        for so in bpy.context.selected_objects:
            if so.type != 'MESH':
                 so.select = False
        if len(bpy.context.selected_objects) >= 1:
            print('Joining mesh objects:' + str(bpy.context.selected_objects))
            bpy.context.scene.objects.active = bpy.context.selected_objects[0]
            bpy.ops.object.join()
            return bpy.context.selected_objects[0]
        return None

    def __find_archimesh_window_mesh(self, parent):
        for window_part in self.__blender_factory.get_children(parent):
            if window_part.name.startswith('Window'):
                return window_part

    def add_portal_lights(self):
        for window_frame in self.__find_glass_frame_objects():
            window_empty = window_frame.parent if window_frame.parent else window_frame
            self.__blender_factory.apply_scale(window_empty)
            self.__blender_factory.apply_rotation_and_scale(window_frame)

            portal_name = window_empty.name + '.portal_light'
            if not bpy.context.scene.objects.get(portal_name):
                print('Adding portal light for ' + window_empty.name)
                self.__add_portal_light(portal_name, window_empty, window_frame)

    def __find_glass_frame_objects(self):
        frame_objects = []
        for glass_object in self.__blender_factory.get_by_material_contains('GLASS'):
            if glass_object.parent:
                for part in self.__blender_factory.get_children(glass_object.parent):
                    if self.__blender_factory.has_material_containing(part, 'FRAME'):
                        frame_objects.append(part)

        return frame_objects

    def __add_portal_light(self, portal_name, window_empty, window_frame):
        # portal light is positioned correctly, even when window_empty or window_frame origins are set wrongly,
        # by making it a child of window_frame, for which its origin is recalculated
        self.__blender_factory.set_origin_to_geometry(window_frame)

        self.__align_frame_to_window_rotation(window_empty, window_frame, invert=True)
        bpy.ops.object.lamp_add(type='AREA')
        area_lamp = bpy.context.scene.objects.active
        area_lamp.name = portal_name
        area_lamp.parent = window_frame
        area_lamp.location[0] = 0
        area_lamp.location[1] = -1 * (window_frame.dimensions[1] / 2 + 0.025)  # move to the outside a bit
        area_lamp.location[2] = 0
        area_lamp.rotation_euler[0] = math.radians(90)  # 90 X rotation points the lamp to the inside
        area_lamp.rotation_euler[1] = 0
        area_lamp.rotation_euler[2] = 0
        bpy.context.object.data.cycles.is_portal = True
        bpy.context.object.data.shape = 'RECTANGLE'
        bpy.context.object.data.size = window_frame.dimensions[0]
        bpy.context.object.data.size_y = window_frame.dimensions[2]
        self.__align_frame_to_window_rotation(window_empty, window_frame, invert=False)

        self.__blender_factory.set_to_layer(area_lamp, Layers.LIGHTS[0])

    def __align_frame_to_window_rotation(self, window_empty, window_frame, invert):
        matrix_world = window_empty.matrix_world
        if invert:
            matrix_world = matrix_world.inverted()

        rotation_matrix = matrix_world.to_quaternion().to_matrix().to_4x4()
        window_frame.matrix_world = self.__apply_rotation_matrix(window_frame.matrix_world, rotation_matrix)
        self.__blender_factory.apply_rotation_and_scale(window_frame)

    def __apply_rotation_matrix(self, target_matrix, new_rotation_matrix):
        location, rotation, scale = target_matrix.decompose()
        location_matrix = Matrix.Translation(location)
        rotation_matrix = rotation.to_matrix().to_4x4()
        scale_matrix = Matrix.Scale(scale[0], 4, (1, 0, 0)) *\
                       Matrix.Scale(scale[1], 4, (0, 1, 0)) *\
                       Matrix.Scale(scale[2], 4, (0, 0, 1))

        return location_matrix * new_rotation_matrix * rotation_matrix * scale_matrix

    def __offset_edges(self, object, width):
        self.__blender_factory.make_active(object)
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.offset_edges(geometry_mode='extrude', width=width, follow_face=True, caches_valid=False)
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')

    def rename_to_material(self):
        for o in self.get_meshes_with_materials():
            o.name = o.material_slots[0].material.name

    def set_fake_user(self):
        for m in bpy.data.materials:
            m.use_fake_user = True

    def replace_duplicate_materials(self):
        print('Replacing duplicate materials')
        for object in bpy.context.scene.objects:
            if object.type == 'MESH':
                bpy.context.scene.objects.active = object
                for material_slot in object.material_slots:
                    material = material_slot.material

                    if material:
                        parts = material.name.split('.')
                        if not material.name.startswith('WINDOW_GLASS_SURROUNDING') and len(parts) > 1 and parts[-1].isdigit():
                            basename = '.'.join(parts[:-1])
                            original_material = bpy.data.materials.get(basename)
                            if original_material:
                                material_slot.material = original_material
                            else:
                                material.name = basename
                    else:
                        print(' - Object {} has no material. Applying OBJECT_DEFAULT'.format(object.name))
                        object_default = bpy.data.materials.get('OBJECT_DEFAULT')
                        material_slot.material = object_default

                bpy.ops.object.select_all(action='DESELECT')



from blender.layers import Layers

import bpy


class Layers:
    # 1-5
    STRUCTURES = [0, 1, 2, 3, 4]

    # 11-15 for structure layers 0-4, 17 for exterior layer 7
    HIGH_RES_OBJECTS = [10, 11, 12, 13, 14, 17]

    # 6
    LIGHTS = [5]

    # 7
    LOW_RES_OBJECTS = [6]

    # 8
    EXTERIOR = [7]

    # 9
    GLASS = [8]

    # 10
    TEMP = [9]

    UNUSED = [15, 16, 18, 19]

    ALL = range(0, 20)

    LIBRARY_LOW_RES_OBJECTS = [1]

    INTERIOR = [i for i in range(0, 20) if i != 7]

    @staticmethod
    def show_all_layers():
        bpy.context.scene.layers = [True] * 20

    @staticmethod
    def get_visible_layers():
        indexes = []
        for index, value in enumerate(bpy.context.scene.layers):
            if value:
                indexes.append(index)
        return indexes

    @staticmethod
    def set_visible_layers(layers):
        layerMask = [False] * 20
        for i in layers:
            layerMask[i] = True
        bpy.context.scene.layers = layerMask

    @staticmethod
    def make_layers_visible(layers):
        for i in layers:
            bpy.context.scene.layers[i] = True

    @staticmethod
    def set_active_layer(layer):
        bpy.context.scene.layers[layer] = True

    @staticmethod
    def get_layer_indexes(obj):
        indexes = []
        for index, value in enumerate(obj.layers):
            if value:
                indexes.append(index)

        return indexes

    @staticmethod
    def set_to_layer(obj, layerIndex):
        layer_mask = [False] * 20
        layer_mask[layerIndex] = True
        obj.layers = layer_mask

        for child in obj.children:
            Layers.set_to_layer(child, layerIndex)

    @staticmethod
    def set_to_layers(obj, layerIndices):
        layer_mask = [False] * 20
        for layerIndex in layerIndices:
            layer_mask[layerIndex] = True
        obj.layers = layer_mask

        for child in obj.children:
            Layers.set_to_layers(child, layerIndices)

    @staticmethod
    def is_on_visible_layer(obj):
        return any(bpy.context.scene.layers[layer] and obj.layers[layer] for layer in range(0, 20))

    @staticmethod
    def is_on_layer(obj, layers):
        return any(obj.layers[layer] for layer in layers)

    @staticmethod
    def is_on_layers(obj, layers):
        layer_indexes = Layers.get_layer_indexes(obj)
        return all(e in layers for e in layer_indexes)

    @staticmethod
    def has_objects_on_layer(layer):
        return any(o.layers[layer] for o in bpy.context.scene.objects)

    @staticmethod
    def has_objects_on_layers(layers):
        return any(Layers.has_objects_on_layer(layer) for layer in layers)

    @staticmethod
    def get_objects_on_layers(layers):
        return [o for o in bpy.data.objects if any(o.layers[l] for l in layers)]


class LayerCombinations:
    HIGH_RES_RENDERING = Layers.STRUCTURES + \
                         Layers.HIGH_RES_OBJECTS + \
                         Layers.GLASS + \
                         Layers.LIGHTS + \
                         Layers.EXTERIOR

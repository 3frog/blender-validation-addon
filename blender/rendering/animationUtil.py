import math
import bpy


class AnimationUtil(object):

    @staticmethod
    def get_camera():
        camera = bpy.data.objects['Camera']
        if not camera:
            raise ValueError('No camera found with name Camera')

        return camera

    @staticmethod
    def activate_camera(camera):
        camera.select = True
        bpy.context.scene.objects.active = camera
        bpy.context.scene.camera = camera

    @staticmethod
    def get_keyframes(camera, start_keyframe=0, end_keyframe=999):
        keyframes = []
        animation_data = camera.animation_data
        if animation_data is not None and animation_data.action is not None:
            for fcu in animation_data.action.fcurves:
                for keyframe in fcu.keyframe_points:
                    x, y = keyframe.co
                    if x not in keyframes:
                        keyframes.append((math.ceil(x)))

        return [keyframe for keyframe in keyframes if keyframe >= start_keyframe and keyframe <= end_keyframe]

    @staticmethod
    def find_next_available_keyframe(camera):
        keyframes = AnimationUtil.get_keyframes(camera)
        if keyframes:
            last_keyframe = keyframes[-1]
            return last_keyframe + 1

        return 1

    @staticmethod
    def clear_all_keyframes(camera):
        camera.animation_data_clear()

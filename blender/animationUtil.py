import math


class AnimationUtil(object):

    @staticmethod
    def get_keyframes(camera):
        keyframes = []
        animation_data = camera.animation_data
        if animation_data is not None and animation_data.action is not None:
            for fcu in animation_data.action.fcurves:
                for keyframe in fcu.keyframe_points:
                    x, y = keyframe.co
                    if x not in keyframes:
                        keyframes.append((math.ceil(x)))

        return keyframes

    @staticmethod
    def find_next_available_keyframe(camera):
        keyframes = AnimationUtil.get_keyframes(camera)
        if keyframes:
            last_keyframe = keyframes[-1]
            return last_keyframe + 1

        return 1

    @staticmethod
    def clear_all_keyframes(camera):
        camera.animation_data_clear()

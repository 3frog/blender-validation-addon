import bpy


def delete(object):
    bpy.ops.object.select_all(action='DESELECT')
    object.select = True
    bpy.ops.object.delete()


def merge(objects):
    bpy.ops.object.select_all(action='DESELECT')
    for object in objects:
        object.select = True

    bpy.context.scene.objects.active = objects[0]
    bpy.ops.object.join()

    return bpy.context.scene.objects.active

import bpy

# TODO: There is almost no state in this class, but it is still sent around the code everywhere. Split into several
# utility classes with static methods
from blender.layers import Layers


class BlenderFactory(object):

    def __init__(self):
        raise ValueError(
            'BlenderFactory is fully static, do not instantiate it. Use BlenderFactory.open_file() instead')

    @staticmethod
    def save(filepath):
        bpy.ops.wm.save_as_mainfile(filepath=filepath, relative_remap=False)

    @staticmethod
    def open_file(filepath):
        bpy.ops.wm.open_mainfile(filepath=filepath)
        BlenderFactory.__set_default_settings()

    @staticmethod
    def __disable_save_versions():
        bpy.context.user_preferences.filepaths.save_version = 0

    @staticmethod
    def __set_default_settings():
        BlenderFactory.__disable_save_versions()
        BlenderFactory.use_autopack(False)

    @staticmethod
    def create_empty(name):
        bpy.ops.object.empty_add()
        bpy.context.active_object.name = name
        return bpy.context.active_object

    @staticmethod
    def add_to_parent(name, parent_name, keep_transform=False):
        bpy.ops.object.select_all(action='DESELECT')
        child_object = bpy.data.objects[name]
        child_object.select = True
        parent_object = bpy.data.objects[parent_name]
        bpy.context.scene.objects.active = parent_object
        bpy.ops.object.parent_set(keep_transform=keep_transform)
        child_object.select = False

    @staticmethod
    def detach_from_parent(obj):
        print('Detaching object {} from its parent'.format(obj.name))
        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        BlenderFactory.make_active(obj)
        bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')

    @staticmethod
    def delete_object(obj):
        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        bpy.ops.object.delete()

    @staticmethod
    def get_material(name):
        return None if name not in bpy.data.materials else bpy.data.materials[name]

    @staticmethod
    def has_material(object, material_name):
        return len(object.material_slots) == 1 and object.material_slots[0].name == material_name

    @staticmethod
    def has_material_any_slot(object, material_name):
        return any(material_name == slot.name for slot in object.material_slots)

    @staticmethod
    def get_used_materials(object):
        # TODO: adapt all other material related methods to check if a slot is actualy used?
        material_names = set()
        if object.type == 'MESH':
            for face in object.data.polygons:
                # this should never fail with an index out of range... but it does
                if face.material_index < len(object.material_slots):
                    slot = object.material_slots[face.material_index]
                    material_names.add(slot.material.name)

        return material_names

    @staticmethod
    def get_material_names(obj):
        return [slot.material.name for slot in obj.material_slots if slot.material] if obj.type == 'MESH' else []

    @staticmethod
    def get_by_material(material_name):
        return [o for o in BlenderFactory.get_visible_meshes() if BlenderFactory.has_material(o, material_name)]

    @staticmethod
    def get_by_material_any_slot(material_name):
        return [o for o in BlenderFactory.get_visible_meshes() if
                BlenderFactory.has_material_any_slot(o, material_name)]

    @staticmethod
    def get_by_material_contains(material_name):
        return [o for o in BlenderFactory.get_visible_meshes() if
                BlenderFactory.has_material_containing(o, material_name)]

    @staticmethod
    def has_material_containing(object, material_name):
        return any(material_name in slot.name for slot in object.material_slots)

    def mark_floorplane(self, obj):
        obj.data['floorPlane'] = True

    @staticmethod
    def get_children(parent):
        return [o for o in bpy.context.scene.objects if o.parent == parent]

    @staticmethod
    def get_by_property(key, value):
        return [o for o in BlenderFactory.get_visible_meshes() if key in o.data and o.data[key] == value]

    @staticmethod
    def remove_all_materials(obj):
        obj.data.materials.clear(update_data=True)

    @staticmethod
    def make_active(obj):
        bpy.context.scene.objects.active = obj

    @staticmethod
    def make_single_user(obj):
        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        BlenderFactory.make_active(obj)
        bpy.ops.object.make_single_user(object=True, obdata=True, material=False, texture=False, animation=False)
        obj.select = False

    @staticmethod
    def needs_triangulation(obj):
        # Note: does not work in edit mode, as mesh.polygons may not be up to date after manipulations
        if obj.type != 'MESH':
            return False

        for polygon in obj.data.polygons:
            if len(polygon.vertices) is not 3:
                return True
        return False

    @staticmethod
    def triangulate(obj):
        bpy.context.scene.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.quads_convert_to_tris(quad_method='BEAUTY', ngon_method='BEAUTY')
        bpy.ops.object.mode_set(mode='OBJECT')

    @staticmethod
    def delete_loose(obj):
        bpy.context.scene.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.delete_loose()
        bpy.ops.object.mode_set(mode='OBJECT')

    @staticmethod
    def create_plane(name, vertices, faces):
        mesh_data = bpy.data.meshes.new(name + '_mesh')
        mesh_data.from_pydata(vertices, [], faces)
        mesh_data.update()

        plane = bpy.data.objects.new(name, mesh_data)
        bpy.context.scene.objects.link(plane)

        return plane

    # Selects given object and all children. Makes given object the active object
    @staticmethod
    def select_hierarchy(parent, additive=False):
        def select_children(parent):
            for child in parent.children:
                child.select = True
                select_children(child)

        if not additive:
            bpy.ops.object.select_all(action='DESELECT')

        parent.select = True
        select_children(parent)
        bpy.context.scene.objects.active = parent

    @staticmethod
    def select_hierarchy_up(obj, additive=False):
        def select_parents(child):
            if child.parent:
                # print('Selecting {}'.format(child.parent))
                child.parent.select = True
                select_parents(child.parent)

        if not additive:
            bpy.ops.object.select_all(action='DESELECT')
        # print('Selecting {}'.format(obj.name))
        obj.select = True
        select_parents(obj)
        bpy.context.scene.objects.active = obj

    @staticmethod
    def find_top_level(objects):
        return [obj for obj in objects if not obj.parent or not obj.parent in objects][0]

    @staticmethod
    def get_visible_meshes():
        return [o for o in bpy.context.scene.objects if
                o.type == 'MESH' and o.is_visible(bpy.context.scene) and Layers.is_on_visible_layer(o)]

    @staticmethod
    def select_object(obj):
        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        BlenderFactory.make_active(obj)

    @staticmethod
    def get_selected_visible_meshes():
        return [o for o in BlenderFactory.get_visible_meshes() if o.select]

    @staticmethod
    def edge_split_all():
        bpy.ops.object.select_all(action='DESELECT')
        for o in BlenderFactory.get_visible_meshes():
            BlenderFactory.edge_split(o)

    @staticmethod
    def edge_split(o, treshold=60):
        print('Edge splitting object ' + o.name)
        BlenderFactory.make_active(o)
        o.select = True
        edge_split = o.modifiers.new('AutomatedEdgeSplit', 'EDGE_SPLIT')
        edge_split.split_angle = 3.14 * treshold / 180
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier='AutomatedEdgeSplit')
        o.select = False

    @staticmethod
    def split_by_loose(o):
        # print("Splitting by loose parts: " + o.name)
        BlenderFactory.select_object(o)
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.separate(type='LOOSE')
        bpy.ops.object.mode_set(mode='OBJECT')
        return BlenderFactory.get_selected_visible_meshes()

    @staticmethod
    def apply_scale(obj):
        if BlenderFactory.__has_nonstandard_scale(obj):
            BlenderFactory.__transform_apply(obj, scale=True)

    @staticmethod
    def apply_rotation_and_scale(obj):
        if BlenderFactory.__has_nonstandard_rotation(obj) or BlenderFactory.__has_nonstandard_scale(obj):
            BlenderFactory.__transform_apply(obj, rotation=True, scale=True)

    @staticmethod
    def __has_nonstandard_scale(obj):
        return any(abs(1 - x) > 0.00001 for x in obj.scale)

    @staticmethod
    def __has_nonstandard_rotation(obj):
        return any(abs(x) > 0.00001 for x in obj.rotation_euler)

    @staticmethod
    def __transform_apply(obj, location=False, rotation=False, scale=False):
        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        BlenderFactory.make_active(obj)
        try:
            bpy.ops.object.transform_apply(location=location, rotation=rotation, scale=scale)
        except RuntimeError:
            print('Could not apply location=' + str(location) + ', rotation=' + str(rotation) +
                  ' and scale=' + str(scale) + ' on object ' + obj.name)
        obj.select = False

    @staticmethod
    def use_autopack(autopack):
        bpy.context.blend_data.use_autopack = autopack

    @staticmethod
    def set_origin_to_geometry(obj):
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.scene.objects.active = obj
        obj.select = True
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
        bpy.ops.object.select_all(action='DESELECT')

    @staticmethod
    def shade_flat_visible_meshes():
        print('Flat shading visible meshes')

        meshes = BlenderFactory.get_visible_meshes()
        smooth_meshes = [m for m in meshes if any(poly.use_smooth for poly in m.data.polygons)]

        print('Flat shading {} of {} meshes'.format(len(smooth_meshes), len(meshes)))

        for m in smooth_meshes:
            print('Flat shading object {}'.format(m.name))
            BlenderFactory.shade_flat(m)

    @staticmethod
    def shade_flat(o):
        for poly in o.data.polygons:
            if poly.use_smooth:
                poly.use_smooth = False

    @staticmethod
    def get_unlinked_objects():
        return list(set(bpy.data.objects) - set(bpy.context.scene.objects))

    @staticmethod
    def get_object_by_name(name):
        return bpy.context.scene.objects[name]

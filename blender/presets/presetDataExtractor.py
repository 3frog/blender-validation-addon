import bpy

from blender.blenderfactory import BlenderFactory
from blender.presets.preset import Preset


class PresetObjectData(object):

    def __init__(self, name, group_name, scale):
        self.name = name
        self.group_name = group_name
        self.scale = scale


class PresetData(object):

    def __init__(self, material_names, object_data):
        self.material_names = material_names
        self.object_data = object_data


def get_preset_data(preset: Preset):
    blender_factory = BlenderFactory(preset.preset_file)

    material_names = list(material.name for material in bpy.data.materials)
    object_data = []

    blender_factory.set_visible_layers([0])
    for obj in blender_factory.get_visible_meshes():
        for group in obj.users_group:
            object_data.append(PresetObjectData(obj.name, group.name, (obj.scale[0], obj.scale[1], obj.scale[2])))

    return PresetData(material_names, object_data)

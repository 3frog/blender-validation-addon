import os


class Preset(object):
    """
    A Preset represents a physical entity, a filesystem directory that can contain one or more types of resources.
    Currently Library (for furniture) and MaterialLibrary (for material definitions) are supported.
    """


    presets_home = None

    def __init__(self, name):
        Preset.__validate_presets_home()
        self.name = name

        self.preset_dir = os.path.join(Preset.presets_home, name)

        self.__preset_file = os.path.join(self.preset_dir, 'preset.blend')
        self.__textures_file = os.path.join(self.preset_dir, 'textures.json')
        self.__material_library_file = os.path.join(self.preset_dir, 'material_library.json')
        self.__materials_file = os.path.join(self.preset_dir, 'materials.json')
        self.__default_materials_file = os.path.join(self.preset_dir, 'default_materials_mapping.json')

        self.__materials_dir = os.path.join(self.preset_dir, 'materials')
        self.__library_dir = os.path.join(self.preset_dir, 'library')
        self.__material_library_dir = os.path.join(self.preset_dir, 'material_library')
        self.__material_library_previews_dir = os.path.join(self.__material_library_dir, 'previews')
        self.__material_library_images_dir = os.path.join(self.__material_library_dir, 'materials')

    @property
    def preset_file(self):
        self.__validate_available()
        return self.__preset_file

    @property
    def textures_file(self):
        self.__validate_available()
        return self.__textures_file

    @property
    def materials_file(self):
        self.__validate_available()
        return self.__materials_file

    @property
    def default_materials_file(self):
        self.__validate_available()
        return self.__default_materials_file

    @property
    def materials_dir(self):
        self.__validate_available()
        return self.__materials_dir

    @property
    def library_dir(self):
        self.__validate_available()
        return self.__library_dir

    @property
    def material_library_dir(self):
        self.__validate_available()
        return self.__material_library_dir

    @property
    def material_library_previews_dir(self):
        self.__validate_available()
        return self.__material_library_previews_dir

    @property
    def material_library_images_dir(self):
        self.__validate_available()
        return self.__material_library_images_dir

    def __str__(self):
        return self.name

    @staticmethod
    def exists(name):
        Preset.__validate_presets_home()
        return os.path.isdir(os.path.join(Preset.presets_home, name))

    @staticmethod
    def __validate_presets_home():
        if not Preset.presets_home:
            raise ValueError('Cannot create Preset object: set Preset.presets_home first')

    def __validate_available(self):
        if not Preset.exists(self.name):
            # imported locally because this preset file is used in the validation addon
            from blender.install.environments import get_current_environment
            if get_current_environment().remote:
                raise ValueError(
                    'Preset {} is not available locally, call Preset.make_available_locally first'.format(self.name))
            else:
                raise ValueError('Preset directory {} does not exist'.format(self.preset_dir))

    @staticmethod
    def get_presets(name=''):
        preset_names = set()
        start_dir = os.path.realpath(os.path.join(Preset.presets_home, name))

        if not os.path.isdir(start_dir) or \
                not os.path.commonpath([Preset.presets_home]) == os.path.commonpath([Preset.presets_home, start_dir]):
            raise ValueError(
                'Preset name {} is not valid: preset directory {} is not a subdirectory of {}'.format(name, start_dir,
                                                                                                      Preset.presets_home))

        start_dir = os.path.abspath(start_dir)

        for dirName, subdirList, fileList in os.walk(start_dir):
            for fname in fileList:
                if fname in ("library.json", "material_library.json"):
                    # remove presets_home + path separator
                    preset_name = os.path.realpath(dirName).split(Preset.presets_home, 1)[1][1:]
                    preset_names.add(preset_name)
        return [Preset(name) for name in preset_names]

    @staticmethod
    def get_deprecated_materials_mapping_file():
        Preset.__validate_presets_home()
        return os.path.join(Preset.presets_home, 'deprecated_materials_mapping.json')

    def make_available_locally(self):
        # Note: does not download recursively. Hopefully not needed

        # imported locally because this preset file is used in the validation addon
        from blender.install.environments import get_current_environment
        env = get_current_environment()

        if not env.remote:
            print('Current environment is not remote, assuming all needed presets are locally available')
        elif not os.path.isdir(self.preset_dir):
            print('Downloading preset {} from {}'.format(self.name, env.name))
            source_path = 'presets/' + self.name

            # imported locally because this preset file is used in the validation addon
            from blender.install.gsutil import Gsutil
            Gsutil.gs_sync_from_remote(env.api_data_bucket, source_path, self.preset_dir)


Preset.presets_home = os.path.realpath(os.environ.get('THREESTATE_PRESETS_HOME'))
if not Preset.presets_home:
    print('THREESTATE_PRESETS_HOME variable not defined, point it to the ThreeState presets root folder')

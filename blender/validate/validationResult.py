from blender.consoleColors import ConsoleColors


class ValidationResult(object):
    ERROR_MESSAGE = ' - ' + ConsoleColors.FAIL + 'ERROR   {} : {}' + ConsoleColors.ENDC
    WARNING_MESSAGE = ' - ' + ConsoleColors.WARNING + 'WARN    {} : {}' + ConsoleColors.ENDC

    def __init__(self, key):
        self.key = key
        self.__errors = []
        self.__warnings = []

    def addError(self, message, obj=None):
        layout = {"message": message, "object": obj}
        self.__errors.append(layout)

    def addWarning(self, message, obj=None):
        layout = {"message": message, "object": obj}
        self.__warnings.append(layout)

    def printValidation(self):
        print()
        print('Validating ' + self.key + ':')
        if self.__errors:
            for error in self.__errors:
                print(ValidationResult.ERROR_MESSAGE.format(error["message"], error["object"] or ''))

        if self.__warnings:
            for warning in self.__warnings:
                print(ValidationResult.WARNING_MESSAGE.format(warning["message"], warning["object"] or ''))

        if not self.__errors and not self.__warnings:
            print(' - ' + ConsoleColors.OKGREEN + 'OK' + ConsoleColors.ENDC)

    def getErrors(self):
        return self.__errors

    def getWarnings(self):
        return self.__warnings

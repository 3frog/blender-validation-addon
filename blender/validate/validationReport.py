from blender.validate.validationResult import ValidationResult


class ValidationReport(object):

    def __init__(self):
        self.validation_results = {}

    def add_validation_result(self, validation_result: ValidationResult):
        self.validation_results[validation_result.key] = validation_result

    def print_validation_results(self, include_ok=True):
        for index, result in self.validation_results.items():
            if include_ok or result.getErrors() or result.getWarnings():
                result.printValidation()

    def get_validation_results(self):
        return self.validation_results

    def has_errors(self):
        return any(result.getErrors() for result in self.validation_results.values())

    def has_warnings(self):
        return any(result.getWarnings() for result in self.validation_results.values())

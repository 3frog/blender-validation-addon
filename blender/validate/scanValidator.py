import os

import bpy

from blender.blenderfactory import BlenderFactory
from blender.designs.camera import Camera
from blender.layers import Layers
from blender.structure.window import Window

from blender.structure.roomLabelMapping import RoomLabelMapping
from blender.validate.validationResult import ValidationResult
from blender.validate.validationReport import ValidationReport

from blender.blenderName import LOW_SUFFIX
from blender.blenderMaterialUtil import BlenderMaterialUtil


class ScanValidator(object):
    validation_result = {}

    INTERIOR_ONLY_MATERIALS = [
        'WALL_DEFAULT',
        'CEILING_DEFAULT',
        'FLOOR_DEFAULT',
        'FLOOR_NON_WALKABLE',
        'DOOR_FRAME',
        'WINDOW_FRAME'
    ]

    def __init__(self, allowed_materials=None, scan=None):
        self.__scan = scan
        self.__allowed_materials = allowed_materials
        self.validation_report = ValidationReport()

    def validate_fixed(self):
        print('Start validating')

        self.__set_visible_layers()

        self.validation_report.add_validation_result(self.__validate_materials(self.__allowed_materials))
        self.validation_report.add_validation_result(self.__validate_images())
        self.validation_report.add_validation_result(self.__validate_surroundings())
        self.validation_report.add_validation_result(self.__validate_object_structures())
        self.validation_report.add_validation_result(self.__validate_object_scale())
        self.validation_report.add_validation_result(self.__validate_window_structures())
        self.validation_report.add_validation_result(self.__validate_used_layers())
        self.validation_report.add_validation_result(self.__validate_cameras())
        self.validation_report.add_validation_result(self.__validate_floor_labels())

        return self.validation_report

    def __set_visible_layers(self):
        all_layers_except_temp = [i for i in Layers.ALL if i not in Layers.TEMP + Layers.EXTERIOR]
        Layers.set_visible_layers(all_layers_except_temp)

    def __validate_materials(self, allowed_structural_materials=None):
        result = ValidationResult('Materials')
        for obj in BlenderFactory.get_visible_meshes():
            self.__validate_materials_for_object(obj, allowed_structural_materials, result)
        return result

    def __validate_materials_for_object(self, obj, allowed_structural_materials, result):
        if not obj.material_slots:
            message = 'Found object without material: ' + obj.name
            result.addError(message, obj)

        for material_slot in obj.material_slots:

            if not material_slot.material:
                result.addWarning('Found empty material slot in object {}'.format(obj.name))
                continue

            material_name_parts = material_slot.name.split('.')
            material_name = material_name_parts[0]

            if allowed_structural_materials and material_name not in allowed_structural_materials:
                message = 'Found unsupported, non-semantic material {} in object {}: '.format(material_name, obj.name)
                result.addError(message, obj)

            interior_structure = not Layers.is_on_layer(obj, Layers.EXTERIOR)
            exterior_structure = not Layers.is_on_layer(obj, Layers.INTERIOR)
            if exterior_structure and material_name in ScanValidator.INTERIOR_ONLY_MATERIALS:
                message = 'Found interior-only material {} on exterior-only object {}'.format(material_name, obj.name)
                # TODO: Warning for now, to not break in-progress scans. Make error later
                result.addWarning(message, obj)

            if interior_structure and material_name.startswith('EXT_') and material_name not in ['EXT_DOOR_FRAME',
                                                                                                 'EXT_WINDOW_FRAME']:
                message = 'Found exterior semantic material {} on non-exterior layer for object {}' \
                    .format(material_name, obj.name)
                result.addWarning(message, obj)

    def __validate_images(self):
        result = ValidationResult('Images')

        for image in bpy.data.images:
            if image.source == 'FILE' and not image.packed_file:
                message = 'Image ' + image.name + ' is not packed'
                result.addWarning(message)

        return result

    def __validate_surroundings(self):
        result = ValidationResult('Surroundings')
        if self.__scan:
            dir_scan_suroundings = self.__scan.dir_scanner_surrounding
        else:
            dir_scan_suroundings = os.path.join(os.path.dirname(os.path.realpath(bpy.data.filepath)), 'surroundings')

        if not os.path.isdir(dir_scan_suroundings):
            message = 'scanner surroundingsdir not available, cannot validate surroundings'
            result.addWarning(message)

        else:
            for window in Window.get_windows():
                surrounding_file_lower = os.path.join(dir_scan_suroundings, window.name + '.jpg')
                surrounding_file_upper = os.path.join(dir_scan_suroundings, window.name + '.JPG')
                if not os.path.isfile(surrounding_file_lower) and not os.path.isfile(surrounding_file_upper):
                    message = 'no surrounding file found for window: ' + window.name
                    result.addWarning(message, window.empty_object)

            window_names = set()
            for file in os.listdir(dir_scan_suroundings):
                window_names.add(os.path.splitext(file)[0])

            for window_name in window_names:
                if window_name not in bpy.context.scene.objects:
                    message = 'no window found for surrounding file: ' + os.path.join(dir_scan_suroundings,
                                                                                      window_name + '.jpg')
                    result.addWarning(message)

        return result

    def __validate_object_structures(self):
        result = ValidationResult('Object structures')
        for obj in BlenderFactory.get_visible_meshes():
            if obj.data and hasattr(obj.data, 'polygons') and len(obj.data.polygons) == 0:
                message = 'found object without faces: ' + obj.name
                result.addError(message, obj)

        return result

    def __validate_object_scale(self):
        result = ValidationResult('Object scales')
        for obj in BlenderFactory.get_visible_meshes():
            if obj.scale[0] < 0 or obj.scale[1] < 0 or obj.scale[2] < 0:
                message = 'found object with a negative scale: ' + obj.name
                result.addError(message, obj)
        return result

    def __validate_window_structures(self):
        result = ValidationResult('Window structures')

        windows = Window.get_windows()
        for window in windows:
            child_material_names = set()
            for child in window.children:
                child_material_names = child_material_names.union(BlenderMaterialUtil.get_used_materials(child))

            if not any('WINDOW_GLASS' in s for s in child_material_names):
                message = 'found window object without WINDOW_GLASS child: ' + window.name + ', used materials: ' + str(
                    child_material_names)
                result.addError(message, window.empty_object)

            if not any('FRAME' in s for s in child_material_names):
                message = 'found window object without FRAME child: ' + window.name + ', used materials: ' + str(
                    child_material_names)
                result.addError(message, window.empty_object)

        window_names = [window.name for window in windows]

        window_glass_children = BlenderMaterialUtil.get_by_material_contains('WINDOW_GLASS')
        for window_child in window_glass_children:
            if not window_child.parent:
                message = 'found WINDOW_GLASS object ' + window_child.name + ' without window parent'
                result.addError(message, window_child)

            elif window_child.parent.name not in window_names:
                message = 'found WINDOW_GLASS object ' + window_child.name + ' with a non window parent: ' + window_child.parent.name
                result.addError(message, window_child)

        window_frame_children = BlenderMaterialUtil.get_by_material_contains('WINDOW_FRAME')
        for window_child in window_frame_children:
            if not window_child.parent:
                message = 'found WINDOW_FRAME object ' + window_child.name + ' without window parent'
                result.addError(message, window_child)

            elif window_child.parent.name not in window_names:
                message = 'found WINDOW_FRAME object ' + window_child.name + ' with a non window parent: ' + window_child.parent.name
                result.addError(message, window_child)

        return result

    def __validate_used_layers(self):
        result = ValidationResult('Layer usage')

        if Layers.has_objects_on_layers(Layers.LOW_RES_OBJECTS):
            message = 'should not have any objects the LOW_RES_OBJECTS layer 7'
            result.addError(message)

        if Layers.has_objects_on_layers(Layers.HIGH_RES_OBJECTS):
            message = 'should not have any objects on the HIGH_RES_OBJECTS layers 11-15 and 18'
            result.addError(message)

        if Layers.has_objects_on_layers(Layers.UNUSED):
            message = 'should not have any objects on the UNUSED layers 16, 17, 19 or 20'
            result.addError(message)

        if any(obj.layers[Layers.LIGHTS[0]] and obj.type == 'MESH' for obj in bpy.context.scene.objects):
            message = 'found objects on the LIGHTS layer'
            result.addError(message)

        return result

    def __validate_cameras(self):
        result = ValidationResult('Camera\'s')
        if not 'Camera' in bpy.data.objects:
            message = 'no camera found with name Camera'
            result.addError(message)
        else:
            camera = Camera()
            keyframes = camera.get_keyframes()

            if not keyframes:
                message = 'no keyframes found on main camera'
                result.addError(message, camera)
            else:
                camera.activate_keyframe(keyframes[0])
                if not camera.is_vertically_straight():
                    # TODO: ERROR for interiors, warning for exteriors
                    result.addWarning('First Camera keyframe must be vertically straight '
                                      '(rotation x,y: 90,0 or -90,180 or -90,-180), '
                                      'but it is at {}'.format(camera.get_x_y_rotation()))

        return result

    def __validate_floor_labels(self):
        result = ValidationResult('Floor labels')
        for floor in BlenderMaterialUtil.get_by_material_contains('FLOOR_DEFAULT'):
            floor_name_parts = floor.name.split('.')
            if len(floor_name_parts) > 1:
                floor_type = floor_name_parts[1]
                if floor_type not in RoomLabelMapping.labels:
                    message = 'unknown floor type ' + floor_type + ', defined in ' + floor.name
                    result.addError(message, floor)

            else:
                message = 'cannot extract floor type from object name: ' + floor.name
                result.addError(message, floor)

        return result

    def __validate_preset_objects(self, objects_data):
        result = ValidationResult('Preset objects')

        Layers.set_visible_layers(Layers.HIGH_RES_OBJECTS)
        for obj in BlenderFactory.get_visible_meshes():
            object_name_parts = obj.name.split('.')
            object_name = object_name_parts[0]
            object_found_in_preset = False

            for group in obj.users_group:
                if LOW_SUFFIX in group.name:
                    message = 'LOW preset object found on high-res layer, ' + 'probably wrongly appended: ' + obj.name
                    result.addError(message, obj)

                group_name_parts = group.name.split('.')
                group_name = group_name_parts[0]

                for object_data in objects_data:
                    if object_data.name.startswith(object_name) and object_data.group_name == group_name:
                        if not (round(obj.scale[0], 4) == round(object_data.scale[0], 4) and
                                round(obj.scale[1], 4) == round(object_data.scale[1], 4) and
                                round(obj.scale[2], 4) == round(object_data.scale[2], 4)):
                            message = obj.name + '-' + group.name + ' scale: ' + str(
                                obj.scale) + ' differs from preset scale: ' + str(object_data.scale)
                            result.addWarning(message, obj)

                        object_found_in_preset = True

            if not object_found_in_preset:
                message = obj.name + ' is placed on a HIGH_RES_OBJECTS layer ' + 'but cannot be found in an existing preset'
                result.addError(message, obj)

        return result

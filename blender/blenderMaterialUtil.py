import bpy
from blender.blenderfactory import BlenderFactory
from blender.layers import Layers


class BlenderMaterialUtil(object):

    @staticmethod
    def get_blender_low_res_materials():
        blender_materials = set()

        Layers.set_visible_layers(Layers.LIBRARY_LOW_RES_OBJECTS)
        objects = [obj for obj in BlenderFactory.get_visible_meshes() if len(obj.material_slots)]

        for library_object in objects:
            for material in library_object.material_slots:
                blender_materials.add(material.material)
        return blender_materials

    @staticmethod
    def change_object_material_targed_by_new(obj, targed_material, new_material):
        for slot in obj.material_slots:
            if slot.material.name == targed_material.name:
                slot.material = new_material

    @staticmethod
    def get_by_material_any_slot(material_name):
        return [o for o in BlenderFactory.get_visible_meshes() if
                BlenderMaterialUtil.has_material_any_slot(o, material_name)]

    @staticmethod
    def get_by_material(material_name):
        return [o for o in BlenderFactory.get_visible_meshes() if BlenderMaterialUtil.has_material(o, material_name)]

    @staticmethod
    def has_material_containing(object, material_name):
        return any(material_name in slot.name for slot in object.material_slots)

    @staticmethod
    def get_by_material_contains(material_name):
        return [o for o in BlenderFactory.get_visible_meshes() if
                BlenderMaterialUtil.has_material_containing(o, material_name)]

    @staticmethod
    def remove_all_materials(obj):
        obj.data.materials.clear(update_data=True)

    @staticmethod
    def split_by_material(o):
        BlenderFactory.make_active(o)
        o.select = True
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.separate(type='MATERIAL')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        o.select = False

    @staticmethod
    def get_all_materials():
        return list(material.name for material in bpy.data.materials)

    @staticmethod
    def get_used_materials(object):
        # TODO: adapt all other material related methods to check if a slot is actualy used?
        material_names = set()
        if object.type == 'MESH':
            for face in object.data.polygons:
                # this should never fail with an index out of range... but it does
                if face.material_index < len(object.material_slots):
                    slot = object.material_slots[face.material_index]
                    material_names.add(slot.material.name)

        return material_names

    @staticmethod
    def has_material(object, material_name):
        return len(object.material_slots) == 1 and object.material_slots[0].name == material_name

    @staticmethod
    def has_material_any_slot(object, material_name):
        return any(material_name == slot.name for slot in object.material_slots)

    @staticmethod
    def get_material(name):
        return None if name not in bpy.data.materials else bpy.data.materials[name]

    @staticmethod
    def remove_by_material(material_name):
        bpy.ops.object.select_all(action='DESELECT')
        for o in BlenderMaterialUtil.get_by_material(material_name):
            o.select = True
        bpy.ops.object.delete()

    @staticmethod
    def move_by_material_to_layer_contains(material_name, layer):
        for o in BlenderMaterialUtil.get_by_material_contains(material_name):
            Layers.set_to_layer(o, layer)

    @staticmethod
    def move_by_material_to_layer(material_name, layer):
        for o in BlenderMaterialUtil.get_by_material(material_name):
            Layers.set_to_layer(o, layer)


    @staticmethod
    def split_visible_meshes_by_material():
        print('Splitting visible meshes by material')
        # BlenderFactory.show_all_layers()
        bpy.ops.object.select_all(action='DESELECT')

        multi_material_objects = (o for o in BlenderFactory.get_visible_meshes() if len(o.material_slots) > 1)
        for o in multi_material_objects:
            print('Splitting by material {} '.format(o.name))
            BlenderMaterialUtil.split_by_material(o)

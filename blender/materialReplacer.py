import functools
import json
import os

import bpy

from blender.blenderfactory import BlenderFactory
from blender.presets.preset import Preset


class MaterialReplacer(object):

    @staticmethod
    def replace_material(src_material_name, target_material_name, validate_replacement=False):
        target_material = BlenderFactory.get_material(target_material_name)

        count = 0
        for o in BlenderFactory.get_by_material_any_slot(src_material_name):
            if not target_material:
                raise ValueError('Could not find replacement material ' + target_material_name)
            for slot in o.material_slots:
                if slot.material.name == src_material_name:
                    count += 1
                    slot.material = target_material

        if validate_replacement and not count:
            raise ValueError('No materials switched from ' + src_material_name + ' to ' + target_material_name)

    @staticmethod
    def replace_materials(mapping_file, preset):
        if not os.path.exists(mapping_file):
            print('Mapping file ' + mapping_file + ' not found, skipping')
            return False

        with open(mapping_file, 'r') as f:
            materials_to_replace = json.load(f)

        used_material_names = list(map(lambda m: m.name, bpy.data.materials))

        materials_replaced = False
        for material_to_replace in materials_to_replace:
            if material_to_replace['material_uuid'] in used_material_names:
                print('Replacing ' + material_to_replace['material_uuid'])
                obsolete_material = bpy.data.materials.get(material_to_replace['material_uuid'])
                obsolete_material.name = obsolete_material.name + '_OBSOLETE'
                obsolete_material.use_fake_user = False

                material = MaterialReplacer.__append_material_from_preset(material_to_replace['replace_by_uuid'], preset)
                if not material:
                    raise ValueError(
                        'Could not replace ' + material_to_replace['material_uuid'] + ' by ' + material_to_replace[
                            'replace_by_uuid'] + '. Material not found in ' + preset.name + ' or base.')

                print('Found target material: ' + material.name)

                material.use_fake_user = True

                if obsolete_material.users > 0:
                    MaterialReplacer.replace_material(obsolete_material.name, material.name, validate_replacement=True)
                    materials_replaced = True

        return materials_replaced

    @staticmethod
    def __append_material_from_preset(material_name, preferred_preset):
        presets_by_priority = [preferred_preset, Preset('base')]

        material = None
        for preset in presets_by_priority:
            bpy.ops.wm.append(filename=material_name, directory=preset.preset_file + '\\Material\\')
            material = bpy.data.materials.get(material_name)
            if not material:
                continue
            else:
                break

        return material

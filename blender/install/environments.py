import os

import sys


class Environment(object):
    def __init__(self, name, api_url: str, static_url: str, rest_host: str, rest_dir, api_data_bucket: str, archive_bucket: str, remote=True):
        self.name = name
        self.api_url = api_url
        self.static_url = static_url
        self.rest_host = rest_host
        self.rest_dir = rest_dir
        self.api_data_bucket = api_data_bucket
        self.archive_bucket = archive_bucket
        self.remote = remote

def get_local_rest_dir():
    dir = os.environ.get('THREESTATE_VIEWER_API_DATA_DIR')
    if not dir:
        print('THREESTATE_VIEWER_API_DATA_DIR variable not defined, point it to the ThreeState viewer api/data folder')
        sys.exit(1)

    if not os.path.isdir(dir):
        print('THREESTATE_VIEWER_API_DATA_DIR points to nonexistent path {}, please create the directory'.format(dir))
        sys.exit(1)

    return dir


ENVIRONMENTS = {}

ENVIRONMENTS['local'] = Environment(
    name='local',
    api_url='http://localhost:4000',
    static_url='http://localhost:4000',
    rest_host='localhost',
    rest_dir=get_local_rest_dir(),
    api_data_bucket='',
    archive_bucket='',
    remote=False,
)

ENVIRONMENTS['beta'] = Environment(
    name='beta',
    api_url='https://beta.3state.immo/api',
    static_url='https://beta.3state.immo/static',
    rest_host='',
    rest_dir='',
    api_data_bucket='3state-beta-storage',
    archive_bucket='3state-scans-beta',
    remote=True,
)

ENVIRONMENTS['platform'] = Environment(
    name='platform',
    api_url='https://platform.3state.immo/api',
    static_url='https://platform.3state.immo/static',
    rest_host='',
    rest_dir='',
    api_data_bucket='3state-platform-storage',
    archive_bucket='3state-scans',
    remote=True,
)

default_environment = os.environ.get('THREESTATE_ENVIRONMENT')
if not default_environment:
    print('No default environment set, assuming local')
    ENVIRONMENTS['current'] = ENVIRONMENTS['local']
elif default_environment not in ENVIRONMENTS:
    raise ValueError('Invalid environment {}!. Valid environments: {}'.format(default_environment, ENVIRONMENTS.keys()))
else:
    print('Default environment set to ' + default_environment)
    ENVIRONMENTS['current'] = ENVIRONMENTS[default_environment]


def set_environment(env):
    if env:
        ENVIRONMENTS['current'] = ENVIRONMENTS[env]
    else:
        print('Env not specified, not setting')

    return env


def get_current_environment() -> Environment:
    return ENVIRONMENTS['current']

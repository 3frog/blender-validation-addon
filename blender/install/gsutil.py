import os
from subprocess import run

from google.cloud import storage

from blender.install.environments import Environment


class Gsutil(object):

    # TODO: This method is almost identical with gs_sync_to_remote, replace its uses with that
    @staticmethod
    def gs_sync_to_google_storage(env: Environment, source_path, target_path):
        command = ['gsutil', '-h', 'Cache-Control: private, max-age=0', '-m', 'rsync', '-rd']
        command += [source_path]
        command += ['gs://' + os.path.join(env.api_data_bucket, target_path) + '/']
        return command

    @staticmethod
    def gs_upload_file_command(env: Environment, source_path, target_path):
        rsync_command = ['gsutil', '-h', 'Cache-Control: private, max-age=0', 'cp']
        rsync_command += [source_path]
        rsync_command += ['gs://' + os.path.join(env.api_data_bucket, target_path) + '/']
        return rsync_command

    @staticmethod
    def gs_upload_file(bucket_name, local_path, remote_path):
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        if os.path.isfile(local_path):
            print('Archiving {}  to {}'.format(local_path, remote_path))
            blob = bucket.blob(remote_path)
            blob.upload_from_filename(local_path)

    @staticmethod
    def gs_sync_from_remote(bucket_name, remote_path, local_path, delete=False, missing_ok=False):
        if missing_ok and not Gsutil.gs_path_exists(bucket_name, remote_path):
            print('Directory does not exist: {}, skipping'.format(remote_path))
            return
        os.makedirs(local_path, exist_ok=True)

        gs_url = Gsutil.gs_create_remote_path(bucket_name, remote_path)
        print('Checking out ' + gs_url + ' to ' + local_path)
        options = ['-m', 'rsync', '-r']
        options += ['-d'] if delete else ''
        run(['gsutil'] + options + [gs_url, local_path], check=True)

    @staticmethod
    def gs_create_remote_path(bucket_name, remote_path):
        return os.path.join('gs://' + bucket_name, remote_path)

    @staticmethod
    def gs_download_file(bucket_name, remote_path, local_path):
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(remote_path)
        if blob:
            print('Checking out {} to {}'.format(remote_path, local_path))
            blob.download_to_filename(local_path)

    @staticmethod
    def gs_sync_to_remote(bucket_name, local_path, remote_path, delete=False, dry_run=False):
        gs_url = Gsutil.gs_create_remote_path(bucket_name, remote_path)
        print('Archiving ' + local_path + ' to ' + gs_url)
        options = ['-h', 'Cache-Control: private, max-age=0', '-m', 'rsync', '-r', '-x', '.*blend1$']
        options += ['-d'] if delete else ''
        options += ['-n'] if dry_run else ''
        run(['gsutil'] + options + [local_path, gs_url], check=True)

    @staticmethod
    def gs_path_exists(bucket_name, path):
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        blobs_iterator = bucket.list_blobs(prefix=path, delimiter='/')
        for blob in blobs_iterator:
            return True
        return bool(blobs_iterator.prefixes)

    @staticmethod
    def gs_list_subfolders(bucket_name, path):
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        blobs_iterator = bucket.list_blobs(prefix=path, delimiter='/')
        for blob in blobs_iterator:
            pass
        return set(map(lambda prefix: prefix.split('/')[-2], blobs_iterator.prefixes))

    @staticmethod
    def gs_get_timestamp(bucket_name, scan_name, path):
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(os.path.join(scan_name, path))
        return float(blob.download_as_string()) if blob else 0
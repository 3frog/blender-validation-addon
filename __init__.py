import bpy
import os
import sys

full_path = os.path.realpath(__file__)
sys.path.append(os.path.dirname(full_path))

from . import addon_updater_ops
from blender.validate.scanValidator import ScanValidator
from blender.validate.validationReport import ValidationReport

bl_info = {
    "name": '3State validator',
    "description": 'This validator tests [Structures, Materials, Camera\'s, Floor labels, Object scale, Layer usage] and must succeed before delivering scan',
    "author": '3Frog',
    # version: don't edit manually
	"version": (1, 1, 2),
    "blender": (2, 7, 9),
    "location": 'View 3D > 3State > validator',
    "warning": '',  # used for warning icon and text in addons panel
    "wiki_url": 'git clone git@bitbucket.org:3frog/blender-validation-addon.git',
    "tracker_url": 'git clone git@bitbucket.org:3frog/blender-validation-addon.git',
    "category": 'Development'
}

validation_report = ValidationReport()


# validator main function
class Validator3State(bpy.types.Operator):
    """3State validator"""  # blender will use this as a tooltip for menu items and buttons.
    bl_idname = 'object.validator'  # unique identifier for buttons and menu items to reference.
    bl_label = '3State Validator'  # display name in the interface.
    bl_options = {'REGISTER'}

    def execute(self, context):
        global validation_report

        startup_blende_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'startup_blend.txt')
        if os.path.isfile(startup_blende_path):
            with open(startup_blende_path) as f:
                allowed_materials = f.read()

        validation_report = ScanValidator(allowed_materials).validate_fixed()
        return {'FINISHED'}


class Focus_object_name(bpy.types.Operator):
    """Focus selected object"""
    bl_idname = 'object.focus_object_name'
    bl_label = 'Focus selected object'
    bl_options = {'REGISTER'}
    objectName = bpy.props.StringProperty()

    def execute(self, context):
        if self.objectName:
            obj = bpy.data.objects[self.objectName]
            bpy.ops.object.select_all(action='DESELECT')
            obj.select = True
            bpy.context.scene.objects.active = obj
            bpy.ops.view3d.view_selected()
        return {'FINISHED'}


class View3DPanel():
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'


# error panel
class VisualizeValidationResult(View3DPanel, bpy.types.Panel):
    bl_label = '3state validator'
    bl_context = 'objectmode'
    bl_category = '3state validator'

    error_text = None  # text area to use for this panel
    object_scales_text = None

    def draw(self, context):
        addon_updater_ops.check_for_update_background()
        global validation_report

        layout = self.layout
        col = layout.column()
        layout.operator('object.validator', text='Validate', icon='POTATO')
        col.separator()
        layout = self.layout

        if validation_report.get_validation_results():
            for validation_index, validation_category in validation_report.get_validation_results().items():
                layout.separator()
                row = layout.row()
                row.label(text=validation_index)
                if validation_category.getErrors():
                    for error in validation_category.getErrors():
                        row = layout.row()
                        if error['object']:
                            focus_object_instance = layout.operator("object.focus_object_name",
                                                                    text=' Error: ' + error['message'], icon='ERROR')
                            focus_object_instance.objectName = error['object'].name
                        else:
                            row.label(text='  ==> Error: ' + error['message'], icon='ERROR')
                if validation_category.getWarnings():
                    for warning in validation_category.getWarnings():
                        row = layout.row()
                        if warning['object']:
                            focus_object_instance = layout.operator("object.focus_object_name",
                                                                    text=' Warning: ' + warning['message'],
                                                                    icon='RADIO')
                            focus_object_instance.objectName = warning['object'].name
                        else:
                            row.label(text='  ==> :error ' + warning['message'], icon='RADIO')
                if not validation_category.getWarnings() and not validation_category.getErrors():
                    row.label(text=" okey")


# validator update panel
class UpdatePreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    auto_check_update = bpy.props.BoolProperty(
        name='Auto-check for Update',
        description='If enabled, auto-check for updates using an interval',
        default=True,
    )
    updater_intrval_months = bpy.props.IntProperty(
        name='Months',
        description='Number of months between checking for updates',
        default=0,
        min=0
    )
    updater_intrval_days = bpy.props.IntProperty(
        name='Days',
        description='Number of days between checking for updates',
        default=0,
        min=0,
        max=31
    )
    updater_intrval_hours = bpy.props.IntProperty(
        name='Hours',
        description='Number of hours between checking for updates',
        default=12,
        min=0,
        max=23
    )
    updater_intrval_minutes = bpy.props.IntProperty(
        name='Minutes',
        description='Number of minutes between checking for updates',
        default=0,
        min=0,
        max=59
    )

    def draw(self, context):
        layout = self.layout
        addon_updater_ops.update_settings_ui(self, context)


def register():
    addon_updater_ops.register(bl_info)
    bpy.utils.register_class(Validator3State)
    bpy.utils.register_class(Focus_object_name)
    bpy.utils.register_module(__name__)


def unregister():
    addon_updater_ops.unregister()
    bpy.utils.unregister_class(Validator3State)
    bpy.utils.unregister_class(Focus_object_name)
    bpy.utils.unregister_module(__name__)


if __name__ == '__main__':
    register()
